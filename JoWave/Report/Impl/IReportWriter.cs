namespace JoWave.Report.Impl {
    public interface IReportWriter {
        void Write(IReport report);
    }
}