namespace JoWave.Report.Impl {
    public interface IReport {
        string GetReportText();
        void AddReportData(ReportLine reportLine);
    }
}