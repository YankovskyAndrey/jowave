using System.Collections.Generic;
using System.Text;
using JoWave.Report.Impl;

namespace JoWave.Report {
    public class Report : IReport {
        public const string HeaderLine = "    File       WAVE     JO-Qext    JO-Qabs    JO-Qsca  ";
        private readonly IList<ReportLine> _reportLines = new List<ReportLine>();

        #region IReport Members

        public string GetReportText() {
            var reportTextBuilder = new StringBuilder();
            reportTextBuilder.AppendLine(HeaderLine);
            foreach (var reportLine in _reportLines) {
                reportTextBuilder.AppendLine(reportLine.AsString());
            }
            return reportTextBuilder.ToString();
        }

        public void AddReportData(ReportLine reportLine) {
            _reportLines.Add(reportLine);
        }

        #endregion
    }
}