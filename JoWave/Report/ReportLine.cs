using System;
using JoWave.Data;

namespace JoWave.Report {
    public struct ReportLine {
        public ReportLine(string fileName, string wave, Jo jo) : this() {
            FileName = fileName;
            Wave = wave;
            Jo = jo;
        }

        public string FileName { get; private set; }
        public string Wave { get; private set; }
        public Jo Jo { get; private set; }

        public string AsString() {
            return String.Join(" ", new[] { FileName, Wave, Jo.Qext, Jo.Qabs, Jo.Qsca });
        }
    }
}