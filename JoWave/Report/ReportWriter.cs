using SystemInterface.IO;
using JoWave.Report.Impl;
using JoWave.Util;

namespace JoWave.Report {
    public class ReportWriter : IReportWriter {
        public const string OutputFileName = "JoWaveReport.txt";

        private readonly IFile _file;
        private readonly ILogger _logger;

        public ReportWriter(ILogger logger, IFile file) {
            _logger = logger;
            _file = file;
        }

        #region IReportWriter Members

        public void Write(IReport report) {
            _logger.Log(Resources.WritingResultsToReportFile);
            var reportText = report.GetReportText();
            _file.WriteAllText(OutputFileName, reportText);
            _logger.Log(Resources.Done);
        }

        #endregion
    }
}