using SystemInterface;
using SystemInterface.IO;
using SystemWrapper;
using SystemWrapper.IO;
using JoWave.Data;
using JoWave.Data.Impl;
using JoWave.Report;
using JoWave.Report.Impl;
using JoWave.Util;
using JoWave.Util.Impl;
using SimpleInjector;

namespace JoWave {
    public class Application {
        public static void Main(string[] args) {
            var container = ConfigureDependencies();
            var applicationEngine = container.GetInstance<IApplicationEngine>();
            applicationEngine.Run();
        }

        private static Container ConfigureDependencies() {
            var container = new Container();
            container.Register<IApplicationEngine, ApplicationEngine>();
            container.Register<ILogger, ConsoleLogger>();
            container.Register<IConsole, ConsoleWrap>();
            container.Register<IDirectory, DirectoryWrap>();
            container.Register<IFile, FileWrap>();
            container.Register<IPath, PathWrap>();
            container.Register<IAvgFileReader, AvgFileReader>();
            container.Register<IReportWriter, ReportWriter>();
            container.Register<IJoWaveDataCollector, JoWaveDataCollector>();
            container.Register<IReport, Report.Report>();
            container.Register<IJoParser, JoParser>();
            container.Register<IWaveParser, WaveParser>();
            return container;
        }
    }
}