using System.Collections.Generic;
using SystemInterface.IO;

namespace JoWave {
    public interface IAvgFileReader {
        IEnumerable<string> GetAllAvgFilesNamesInCurrentDirectory();
        IStreamReader Read(string avgFileName);
    }
}