using System.Collections.Generic;
using System.Linq;
using SystemInterface.IO;
using JoWave.Util;

namespace JoWave {
    public class AvgFileReader : IAvgFileReader {
        public const string AvgExtensionSearchPattern = "*.avg";

        private readonly IDirectory _directory;
        private readonly IFile _file;
        private readonly ILogger _logger;
        private readonly IPath _path;

        public AvgFileReader(ILogger logger, IDirectory directory, IFile file, IPath path) {
            _logger = logger;
            _directory = directory;
            _file = file;
            _path = path;
        }

        #region IAvgFileReader Members

        public IEnumerable<string> GetAllAvgFilesNamesInCurrentDirectory() {
            _logger.Log(Resources.SearchingOfAllAvgFilesInCurrentDirectory);
            var currentDirectory = _directory.GetCurrentDirectory();
            return _directory.GetFiles(currentDirectory, AvgExtensionSearchPattern).Select(x => _path.GetFileName(x));
        }

        public IStreamReader Read(string avgFileName) {
            return _file.OpenText(avgFileName);
        }

        #endregion
    }
}