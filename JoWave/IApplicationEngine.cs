namespace JoWave {
    internal interface IApplicationEngine {
        void Run();
    }
}