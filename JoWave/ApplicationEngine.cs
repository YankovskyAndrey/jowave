using System;
using System.Linq;
using JoWave.Data;
using JoWave.Report;
using JoWave.Report.Impl;
using JoWave.Util;

namespace JoWave {
    public class ApplicationEngine : IApplicationEngine {
        private readonly IAvgFileReader _avgFileReader;
        private readonly IJoWaveDataCollector _joWaveDataCollector;
        private readonly ILogger _logger;
        private readonly IReport _report;
        private readonly IReportWriter _reportWriter;

        public ApplicationEngine(IReport report, ILogger logger, IAvgFileReader avgFileReader, IJoWaveDataCollector joWaveDataCollector, IReportWriter reportWriter) {
            _report = report;
            _logger = logger;
            _avgFileReader = avgFileReader;
            _reportWriter = reportWriter;
            _joWaveDataCollector = joWaveDataCollector;
        }

        #region IApplicationEngine Members

        public void Run() {
            _logger.Log(Resources.Title);
            _logger.Log(Resources.DevelopedBy);
            var avgFiles = _avgFileReader.GetAllAvgFilesNamesInCurrentDirectory().ToList();
            if (!avgFiles.Any()) {
                _logger.Log(Resources.NoAvgFilesFoundInCurrentDirectory);
            } else {
                _logger.Log(String.Format(Resources.FilesFound, avgFiles.Count()));
                _logger.Log(Resources.CollectingDataFromThisFiles);
                foreach (var avgFileName in avgFiles) {
                    using (var avgFileStreamReader = _avgFileReader.Read(avgFileName)) {
                        var data = _joWaveDataCollector.GetData(avgFileStreamReader);
                        var reportLine = new ReportLine(avgFileName, data.Wave, data.Jo);
                        _report.AddReportData(reportLine);
                    }
                }
                _reportWriter.Write(_report);
            }
        }

        #endregion
    }
}