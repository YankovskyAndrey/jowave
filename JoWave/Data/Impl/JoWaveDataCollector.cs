using System.Collections.Generic;
using System.Linq;
using SystemInterface.IO;

namespace JoWave.Data.Impl {
    public class JoWaveDataCollector : IJoWaveDataCollector {
        private readonly IJoParser _joParser;
        private readonly IWaveParser _waveParser;

        public JoWaveDataCollector(IWaveParser waveParser, IJoParser joParser) {
            _waveParser = waveParser;
            _joParser = joParser;
        }

        #region IJoWaveDataCollector Members

        public JoWaveData GetData(IStreamReader avgDataStreamReader) {
            var joWaveData = new JoWaveData();
            var dataParsers = new List<IDataParser> { _waveParser, _joParser };
            string line;
            while (dataParsers.Any() && (line = avgDataStreamReader.ReadLine()) != null) {
                foreach (var dataParser in dataParsers) {
                    if (dataParser.Match(line)) {
                        dataParser.Parse(line, ref joWaveData);
                        dataParsers.Remove(dataParser);
                        break;
                    }
                }
            }
            return joWaveData;
        }

        #endregion
    }
}