using System.Text.RegularExpressions;

namespace JoWave.Data.Impl {
    public class WaveParser : IWaveParser {
        #region IWaveParser Members

        public bool Match(string str) {
            return str.TrimStart(' ').StartsWith("WAVE");
        }

        public void Parse(string str, ref JoWaveData data) {
            //SAMPLE WAVE LINE:
            //  WAVE=      0.200000 = wavelength (in vacuo, physical units)
            var waveMatch = Regex.Match(str, @"[0-9.]+");
            data.Wave = waveMatch.Groups[0].Value;
        }

        #endregion
    }
}