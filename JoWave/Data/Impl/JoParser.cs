using System.Text.RegularExpressions;

namespace JoWave.Data.Impl {
    public class JoParser : IJoParser {
        #region IJoParser Members

        public bool Match(string str) {
            return str.TrimStart(' ').StartsWith("JO");
        }

        public void Parse(string str, ref JoWaveData data) {
            //SAMPLE JO LINE
            // JO=1:  2.4993E+00 1.1977E+00 1.3016E+00
            var joPartsMatches = Regex.Matches(str, @"[0-9.E+-]{2,}");
            var joQext = joPartsMatches[0].Groups[0].Value;
            var joQabs = joPartsMatches[1].Groups[0].Value;
            var joQsca = joPartsMatches[2].Groups[0].Value;
            data.Jo = new Jo(joQext, joQabs, joQsca);
        }

        #endregion
    }
}