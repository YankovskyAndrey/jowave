using SystemInterface.IO;
using JoWave.Data.Impl;

namespace JoWave.Data {
    public interface IJoWaveDataCollector {
        JoWaveData GetData(IStreamReader avgDataStreamReader);
    }
}