namespace JoWave.Data {
    public struct JoWaveData {
        public string Wave { get; set; }
        public Jo Jo { get; set; }
    }
}