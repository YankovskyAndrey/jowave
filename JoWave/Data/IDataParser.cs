using JoWave.Data.Impl;

namespace JoWave.Data {
    public interface IDataParser {
        bool Match(string str);
        void Parse(string str, ref JoWaveData joWaveData);
    }
}