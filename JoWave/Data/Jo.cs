namespace JoWave.Data {
    public struct Jo {
        public Jo(string qext, string qabs, string qsca) : this() {
            Qext = qext;
            Qabs = qabs;
            Qsca = qsca;
        }

        public string Qext { get; private set; }
        public string Qabs { get; private set; }
        public string Qsca { get; private set; }
    }
}