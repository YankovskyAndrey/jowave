namespace JoWave.Util {
    public interface ILogger {
        void Log(string message);
    }
}