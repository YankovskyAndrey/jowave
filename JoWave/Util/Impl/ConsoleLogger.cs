using SystemInterface;

namespace JoWave.Util.Impl {
    public class ConsoleLogger : ILogger {
        private readonly IConsole _console;

        public ConsoleLogger(IConsole console) {
            _console = console;
        }

        #region ILogger Members

        public void Log(string message) {
            _console.WriteLine(message);
        }

        #endregion
    }
}