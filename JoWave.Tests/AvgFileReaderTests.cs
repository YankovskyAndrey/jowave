﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SystemInterface.IO;
using SystemWrapper.IO;
using JoWave.Util;
using Moq;
using Xunit;

namespace JoWave.Tests {
    public class AvgFileReaderTests {
        [Fact]
        public void GetAllAvgFilesNamesInCurrentDirectoryTest() {
            // Given
            const string currentDirectory = @"C:\Users\John\";
            const string firstAvgFullPath = @"C:\Users\John\first.avg";
            const string secondAvgFullPath = @"C:\Users\John\second.avg";
            const string thirdAvgFullPath = @"C:\Users\John\third.avg";
            const string firstAvgFileName = "first.avg";
            const string secondAvgFileName = "second.avg";
            const string thirdAvgFileName = "third.avg";
            var avgFilesInCurrentDirectory = new[] { firstAvgFullPath, secondAvgFullPath, thirdAvgFullPath };
            var mockLogger = new Mock<ILogger>(MockBehavior.Strict);
            var mockDirectory = new Mock<IDirectory>(MockBehavior.Strict);
            var mockFile = new Mock<IFile>(MockBehavior.Strict);
            var mockPath = new Mock<IPath>(MockBehavior.Strict);
            var avgFileReader = new AvgFileReader(mockLogger.Object, mockDirectory.Object, mockFile.Object, mockPath.Object);

            // Expect
            mockLogger.Setup(x => x.Log(Resources.SearchingOfAllAvgFilesInCurrentDirectory));
            mockDirectory.Setup(x => x.GetCurrentDirectory()).Returns(currentDirectory);
            mockDirectory.Setup(x => x.GetFiles(currentDirectory, AvgFileReader.AvgExtensionSearchPattern)).Returns(avgFilesInCurrentDirectory);
            mockPath.Setup(x => x.GetFileName(firstAvgFullPath)).Returns(firstAvgFileName);
            mockPath.Setup(x => x.GetFileName(secondAvgFullPath)).Returns(secondAvgFileName);
            mockPath.Setup(x => x.GetFileName(thirdAvgFullPath)).Returns(thirdAvgFileName);

            // When
            var avgFilesNames = avgFileReader.GetAllAvgFilesNamesInCurrentDirectory().ToList();

            // Then
            mockLogger.VerifyAll();
            mockDirectory.VerifyAll();
            mockFile.VerifyAll();
            mockPath.VerifyAll();
            Assert.Equal(new List<string> { firstAvgFileName, secondAvgFileName, thirdAvgFileName }, avgFilesNames.ToList());
        }

        [Fact]
        public void ReadTest() {
            // Given
            const string filePath = "file.avg";
            const string expectedContent = "first avg file content";
            var firstAvgFileBytes = Encoding.UTF8.GetBytes(expectedContent);
            var memoryStream = new MemoryStream(firstAvgFileBytes);
            var expectedStreamReader = new StreamReaderWrap(memoryStream);
            var mockLogger = new Mock<ILogger>(MockBehavior.Strict);
            var mockDirectory = new Mock<IDirectory>(MockBehavior.Strict);
            var mockFile = new Mock<IFile>(MockBehavior.Strict);
            var mockPath = new Mock<IPath>(MockBehavior.Strict);
            var avgFileReader = new AvgFileReader(mockLogger.Object, mockDirectory.Object, mockFile.Object, mockPath.Object);

            // Expect
            mockFile.Setup(x => x.OpenText(filePath)).Returns(expectedStreamReader);

            // When
            var actualFileContent = avgFileReader.Read(filePath);

            // Then
            mockLogger.VerifyAll();
            mockDirectory.VerifyAll();
            mockFile.VerifyAll();
            mockPath.VerifyAll();
            Assert.Equal(expectedContent, actualFileContent.ReadToEnd());

            // After
            memoryStream.Close();
        }
    }
}