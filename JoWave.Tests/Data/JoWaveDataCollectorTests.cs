﻿using System;
using System.IO;
using System.Text;
using SystemInterface.IO;
using SystemWrapper.IO;
using JoWave.Data.Impl;
using Moq;
using Xunit;

namespace JoWave.Tests.Data {
    public class JoWaveDataCollectorTests {
        [Fact]
        public void GetDataTest() {
            // Given
            var mockAvgDataStreamReader = new Mock<IStreamReader>(MockBehavior.Strict);
            var mockWaveParser = new Mock<WaveParser>(MockBehavior.Strict);
            var mockJoParser = new Mock<JoParser>(MockBehavior.Strict);
            var joWaveDataCollector = new JoWaveDataCollector(mockWaveParser.Object, mockJoParser.Object);

            // Expect
            mockAvgDataStreamReader.Setup(x => x.ReadLine()).Returns("first line which doesn't match anything");
            //mockWaveParser.Setup(x => x.Match())
            // When
            //joWaveDataCollector.GetData(mockAvgDataStreamReader);
            // Then
        }
    }
}