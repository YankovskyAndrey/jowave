using JoWave.Data;
using JoWave.Data.Impl;
using Xunit;

namespace JoWave.Tests.Data {
    public class WaveParserTests {
        [Fact]
        public void MatchTest() {
            var waveParser = new WaveParser();
            Assert.True(waveParser.Match("WAVE brave"));
            Assert.True(waveParser.Match("    WAVE brave"));
            Assert.False(waveParser.Match("WAV brave"));
            Assert.False(waveParser.Match("\tWAVE brave"));
            Assert.False(waveParser.Match("brave"));
            Assert.False(waveParser.Match("wave"));
            Assert.False(waveParser.Match("wAVE"));
            Assert.False(waveParser.Match(""));
            Assert.False(waveParser.Match("           "));
        }

        [Fact]
        public void ParseTest() {
            var waveParser = new WaveParser();

            var joWaveData = new JoWaveData();

            waveParser.Parse("WAVE=      0.927273 = wavelength (in vacuo, physical units)", ref joWaveData);
            Assert.Equal("0.927273", joWaveData.Wave);

            waveParser.Parse("WAVE=      0.967677 = wavelength (in vacuo, physical units)", ref joWaveData);
            Assert.Equal("0.967677", joWaveData.Wave);
        }
    }
}