using JoWave.Data;
using JoWave.Data.Impl;
using Xunit;

namespace JoWave.Tests.Data {
    public class JoParserTests {
        [Fact]
        public void MatchTest() {
            var joParser = new JoParser();
            Assert.True(joParser.Match("JO Foo"));
            Assert.True(joParser.Match("    JO brave"));
            Assert.False(joParser.Match("J brave"));
            Assert.False(joParser.Match("\tJO brave"));
            Assert.False(joParser.Match("brave"));
            Assert.False(joParser.Match("jo"));
            Assert.False(joParser.Match("jO"));
            Assert.False(joParser.Match(""));
            Assert.False(joParser.Match("           "));
        }

        [Fact]
        public void ParseTest() {
            var joParser = new JoParser();

            var joWaveData = new JoWaveData();

            joParser.Parse("JO=1:  3.3298E+00 1.6296E+00 1.7002E+00", ref joWaveData);
            Assert.Equal(new Jo("3.3298E+00", "1.6296E+00", "1.7002E+00"), joWaveData.Jo);

            joParser.Parse("JO=1:  4.4103E-00 2.5184E-00 1.8920E-00", ref joWaveData);
            Assert.Equal(new Jo("4.4103E-00", "2.5184E-00", "1.8920E-00"), joWaveData.Jo);

            joParser.Parse("JO=1:  3.0798E-00 1.4578E+00 1.6220E+00", ref joWaveData);
            Assert.Equal(new Jo("3.0798E-00", "1.4578E+00", "1.6220E+00"), joWaveData.Jo);
        }
    }
}