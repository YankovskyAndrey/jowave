﻿using System;
using System.IO;
using JoWave.Report;
using Xunit;

namespace JoWave.Tests {
    public class ApplicationTests {
        [Fact]
        public void MainTest() {
            // Expect
            var expectedReportContent = "    File       WAVE     JO-Qext    JO-Qabs    JO-Qsca  " + Environment.NewLine +
                                        "w090r000.avg 0.927273 3.0798E+00 1.4578E+00 1.6220E+00" + Environment.NewLine +
                                        "w091r000.avg 0.935354 3.3298E+00 1.6296E+00 1.7002E+00" + Environment.NewLine +
                                        "w095r000.avg 0.967677 4.4103E+00 2.5184E+00 1.8920E+00" + Environment.NewLine +
                                        "w096r000.avg 0.975758 4.6068E+00 2.7302E+00 1.8766E+00" + Environment.NewLine;

            // When
            Application.Main(null);

            // Then
            var reportContent = File.ReadAllText(ReportWriter.OutputFileName);
            Assert.Equal(expectedReportContent, reportContent);
        }
    }
}