using JoWave.Data;
using JoWave.Report;
using Xunit;

namespace JoWave.Tests.Report {
    public class ReportLineTests {
        [Fact]
        public void AsStringTest() {
            // Given
            var reportLine = new ReportLine("fileName", "wave", new Jo("joQext", "joQabs", "joQsca"));

            // Expect
            const string expected = "fileName wave joQext joQabs joQsca";

            // When
            var actual = reportLine.AsString();

            // Then
            Assert.Equal(expected, actual);
        }
    }
}