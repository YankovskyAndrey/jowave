using System;
using JoWave.Data;
using JoWave.Report;
using Xunit;

namespace JoWave.Tests.Report {
    public class ReportTests {
        private readonly string _reportHeaderLineWithNewLine = JoWave.Report.Report.HeaderLine + Environment.NewLine;

        [Fact]
        public void AddReportDataTest() {
            // Given
            var sampleDataReportLine = new ReportLine("1", "2", new Jo("3", "4", "5"));
            var report = new JoWave.Report.Report();

            // Expect
            var expected = _reportHeaderLineWithNewLine + "1 2 3 4 5" + Environment.NewLine;

            // When
            report.AddReportData(sampleDataReportLine);

            // Then
            Assert.Equal(expected, report.GetReportText());
        }

        [Fact]
        public void GetReportTextForEmptyReportTest() {
            // Given
            var report = new JoWave.Report.Report();

            // Expect
            var expected = _reportHeaderLineWithNewLine;

            // When
            var actual = report.GetReportText();

            // Then
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetReportTextAfterAddingOneEmptyLineTest() {
            // Given
            var report = new JoWave.Report.Report();
            var emptyDataReportLine = new ReportLine("", "", new Jo("", "", ""));
            report.AddReportData(emptyDataReportLine);

            // Expect
            var expected = _reportHeaderLineWithNewLine + "    " + Environment.NewLine;

            // When
            var actual = report.GetReportText();

            // Then
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetReportTextAfterAddingOneNonEmptyLineTest() {
            // Given
            var report = new JoWave.Report.Report();
            var sampleDataReportLine = new ReportLine("1", "2", new Jo("3", "4", "5"));
            report.AddReportData(sampleDataReportLine);

            // Expect
            var expected = _reportHeaderLineWithNewLine + "1 2 3 4 5" + Environment.NewLine;

            // When
            var actual = report.GetReportText();

            // Then
            Assert.Equal(expected, actual);
        }
    }
}