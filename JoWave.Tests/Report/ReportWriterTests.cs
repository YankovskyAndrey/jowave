using SystemInterface.IO;
using JoWave.Report;
using JoWave.Report.Impl;
using JoWave.Util;
using Moq;
using Moq.Sequences;
using Xunit;

namespace JoWave.Tests.Report {
    public class ReportWriterTests {
        [Fact]
        public void WriteTest() {
            using (Sequence.Create()) {
                // Given
                const string reportText = "report text";
                var mockLogger = new Mock<ILogger>(MockBehavior.Strict);
                var mockFile = new Mock<IFile>(MockBehavior.Strict);
                var mockJoWaveReport = new Mock<IReport>(MockBehavior.Strict);
                var reportWriter = new ReportWriter(mockLogger.Object, mockFile.Object);

                // Expect
                mockLogger.Setup(x => x.Log(Resources.WritingResultsToReportFile)).InSequence();
                mockLogger.Setup(x => x.Log(Resources.Done)).InSequence();
                mockFile.Setup(x => x.WriteAllText(ReportWriter.OutputFileName, reportText));
                mockJoWaveReport.Setup(x => x.GetReportText()).Returns(reportText);

                // When
                reportWriter.Write(mockJoWaveReport.Object);

                // Then
                mockLogger.VerifyAll();
                mockFile.VerifyAll();
                mockJoWaveReport.VerifyAll();
            }
        }
    }
}