﻿using System.Collections.Generic;
using SystemInterface.IO;
using JoWave.Data;
using JoWave.Report;
using JoWave.Report.Impl;
using JoWave.Util;
using Moq;
using Xunit;

namespace JoWave.Tests {
    public class ApplicationEngineTests {
        [Fact]
        public void RunForDirectoryWithSomeAvgFilesTest() {
            // Given
            const string firstAvgFileName = "firstFile.avg";
            const string secondAvgFileName = "secondFile.avg";
            const int filesCount = 2;
            var avgFilesInCurrentDirectory = new List<string> { firstAvgFileName, secondAvgFileName };
            var mockFirstAvgFileStreamReader = new Mock<IStreamReader>();
            var mockSecondAvgFileStreamReader = new Mock<IStreamReader>();
            var firstJoWaveData = new JoWaveData { Jo = new Jo("1", "2", "3"), Wave = "wave1" };
            var secondJoWaveData = new JoWaveData { Jo = new Jo("4", "5", "6"), Wave = "wave2" };
            var firstReportLine = new ReportLine(firstAvgFileName, firstJoWaveData.Wave, firstJoWaveData.Jo);
            var secondReportLine = new ReportLine(secondAvgFileName, secondJoWaveData.Wave, secondJoWaveData.Jo);
            var mockReport = new Mock<IReport>(MockBehavior.Strict);
            var mockLogger = new Mock<ILogger>(MockBehavior.Strict);
            var mockAvgFileReader = new Mock<IAvgFileReader>(MockBehavior.Strict);
            var mockJoWaveDataCollector = new Mock<IJoWaveDataCollector>(MockBehavior.Strict);
            var mockReportWriter = new Mock<IReportWriter>(MockBehavior.Strict);
            var applicationEngine = new ApplicationEngine(mockReport.Object, mockLogger.Object, mockAvgFileReader.Object, mockJoWaveDataCollector.Object, mockReportWriter.Object);

            // Expect
            mockLogger.Setup(x => x.Log(Resources.Title));
            mockLogger.Setup(x => x.Log(Resources.DevelopedBy));
            mockLogger.Setup(x => x.Log(string.Format(Resources.FilesFound, filesCount)));
            mockLogger.Setup(x => x.Log(Resources.CollectingDataFromThisFiles));
            mockAvgFileReader.Setup(x => x.GetAllAvgFilesNamesInCurrentDirectory()).Returns(avgFilesInCurrentDirectory);
            mockAvgFileReader.Setup(x => x.Read(firstAvgFileName)).Returns(mockFirstAvgFileStreamReader.Object);
            mockAvgFileReader.Setup(x => x.Read(secondAvgFileName)).Returns(mockSecondAvgFileStreamReader.Object);
            mockJoWaveDataCollector.Setup(x => x.GetData(mockFirstAvgFileStreamReader.Object)).Returns(firstJoWaveData);
            mockJoWaveDataCollector.Setup(x => x.GetData(mockSecondAvgFileStreamReader.Object)).Returns(secondJoWaveData);
            mockReport.Setup(x => x.AddReportData(firstReportLine));
            mockReport.Setup(x => x.AddReportData(secondReportLine));
            mockReportWriter.Setup(x => x.Write(mockReport.Object));

            // When
            applicationEngine.Run();

            // Then
            mockReport.VerifyAll();
            mockLogger.VerifyAll();
            mockAvgFileReader.VerifyAll();
            mockJoWaveDataCollector.VerifyAll();
            mockReportWriter.VerifyAll();
        }

        [Fact]
        public void RunForDirectoryWithoutAnyAvgFilesTest() {
            // Given
            var emptyList = new List<string>();
            var mockReport = new Mock<IReport>(MockBehavior.Strict);
            var mockLogger = new Mock<ILogger>(MockBehavior.Strict);
            var mockAvgFileReader = new Mock<IAvgFileReader>(MockBehavior.Strict);
            var mockJoWaveDataCollector = new Mock<IJoWaveDataCollector>(MockBehavior.Strict);
            var mockReportWriter = new Mock<IReportWriter>(MockBehavior.Strict);
            var applicationEngine = new ApplicationEngine(mockReport.Object, mockLogger.Object, mockAvgFileReader.Object, mockJoWaveDataCollector.Object, mockReportWriter.Object);

            // Expect
            mockLogger.Setup(x => x.Log(Resources.Title));
            mockLogger.Setup(x => x.Log(Resources.DevelopedBy));
            mockLogger.Setup(x => x.Log(Resources.NoAvgFilesFoundInCurrentDirectory));
            mockAvgFileReader.Setup(x => x.GetAllAvgFilesNamesInCurrentDirectory()).Returns(emptyList);

            // When
            applicationEngine.Run();

            // Then
            mockReport.VerifyAll();
            mockLogger.VerifyAll();
            mockLogger.VerifyAll();
            mockAvgFileReader.VerifyAll();
            mockJoWaveDataCollector.VerifyAll();
            mockReportWriter.VerifyAll();
        }
    }
}