using SystemInterface;
using JoWave.Util.Impl;
using Moq;
using Xunit;

namespace JoWave.Tests.Util {
    public class ConsoleLoggerTests {
        [Fact]
        public void LogTest() {
            // Given
            const string loggedText = "logged text";
            var mockConsole = new Mock<IConsole>(MockBehavior.Strict);
            var consoleLogger = new ConsoleLogger(mockConsole.Object);

            // Expect
            mockConsole.Setup(x => x.WriteLine(loggedText));

            // When
            consoleLogger.Log(loggedText);

            // Then
            mockConsole.VerifyAll();
        }
    }
}